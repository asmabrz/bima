close all
%Ouverture de l'image 
[I,n,m]=ouvrirImage('../Images/a.png');
 
%Visualisation de l'image initiale
figure, imshow(I);

%transformé de Fourier de l’image initiale 
If1=compute_FT(I);

%Visualisation du spectres normalisés
Ifv_log1=fftshift(to_visualize_TF_log(If1));
Ifv_log1_normalized=normaliseImage(Ifv_log1,0,1);

figure,imagesc(Ifv_log1_normalized);

