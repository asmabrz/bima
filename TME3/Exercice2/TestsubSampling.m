%Ouverture de l'image 
[I,n,m]=ouvrirImage('../Images/barbara.png');
 
%Visualisation de l'image initiale
figure, imshow(I);

%transformé de Fourier de l’image initiale 
If1=compute_FT(I);

%Visualisation du spectres normalisés
Ifv_log1=fftshift(to_visualize_TF_log(If1));
Ifv_log1_normalized=normaliseImage(Ifv_log1,0,1);

figure,imagesc(Ifv_log1_normalized);

J=I;

%Sous-́echantillonnage de l'image
for i=1:9
	[ J ] = subSampling2( J );

	%Visualisation de l'image
	display(size(J,1)) %puissance de 2
	figure, imshow(J/255);

	%transformé de Fourier de l’image initiale 
	If2=compute_FT(J);

	%Visualisation du spectres normalisés
	Ifv_log2=fftshift(to_visualize_TF_log(If2));
	Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);

	figure,imagesc(Ifv_log2_normalized);

end