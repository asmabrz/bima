function J=normaliseImage(I,k1,k2)
J=(I-min(I)).*((k2-k1)./(max(I)-min(I)))+k1;
end
