amplitude=10;
theta=45;
TO=64;
Te=1; 
taille=512;
%Question 4image_normalized5=normaliseImage(image5,0,1);

%Visualisation de l'image échantillonnée
figure,imshow(image_normalized5);

%Calcul de la TF de image5
If5=compute_FT(image5);

%Visualisation du spectres normalisés
Ifv_log5=fftshift(to_visualize_TF_log(If5));
Ifv_log5_normalized=normaliseImage(Ifv_log5,0,1);
figure,imshow(Ifv_log5_normalized);
figure,imagesc(Ifv_log5_normalized);


%reconstruction de l'image
imager5 = reconstruction(image5,1/Te,size(image5,1));
%Visualisation de Image5 reconstruite


%Calcul de la TF de imager5
Ifr5=compute_FT(imager5);

%Visualisation du spectres normalisés
Ifv_logr5=fftshift(to_visualize_TF_log(Ifr5));
Ifv_logr5_normalized=normaliseImage(Ifv_logr5,0,1);

figure,imagesc(Ifv_logr5_normalized);


eps = erreur(imager5,image5,10)




Fe=(3/2)*Fm;

image5 = sinusoid2d(amplitude, theta,taille, TO,1/Fe);
image_normalized5=normaliseImage(image5,0,1);

%Visualisation de l'image échantillonnée
figure,imshow(image_normalized5);

%Calcul de la TF de image5
If5=compute_FT(image5);

%Visualisation du spectres normalisés
Ifv_log5=fftshift(to_visualize_TF_log(If5));
Ifv_log5_normalized=normaliseImage(Ifv_log5,0,1);
figure,imshow(Ifv_log5_normalized);
figure,imagesc(Ifv_log5_normalized);


%reconstruction de l'image
imager5 = reconstruction(image5,1/Te,size(image5,1));
%Visualisation de Image5 reconstruite


%Calcul de la TF de imager5
Ifr5=compute_FT(imager5);

%Visualisation du spectres normalisés
Ifv_logr5=fftshift(to_visualize_TF_log(Ifr5));
Ifv_logr5_normalized=normaliseImage(Ifv_logr5,0,1);

figure,imagesc(Ifv_logr5_normalized);


eps = erreur(imager5,image5,10)



