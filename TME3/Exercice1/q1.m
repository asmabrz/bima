%1) génération de s(t,u), échantillonnage de la fonction
amplitude=10;
theta=45;
TO=64;
Te=1; 
taille=512;  
image1 = sinusoid2d(amplitude, theta,taille, TO,Te);
image_normalized1=normaliseImage(image1,0,1);

%Visualisation de l'image
imshow(image_normalized1);
