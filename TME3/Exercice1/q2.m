amplitude=10;
theta=45;
TO=64;
Te=1; 
taille=512;

%2Calcul des fréquences max u et t
Fm=0.85/64;

%2a) Echantillonnage de s(y,u) avec fe=16
Fe=16*Fm;

image2 = sinusoid2d(amplitude, theta,taille, TO,1/Fe);
image_normalized2=normaliseImage(image2,0,1);

%Visualisation de l'image échantillonnée
figure,imshow(image_normalized2);

%2b)Calcul de la TF de image2
If2=compute_FT(image2);

%Calcul du spectre de IF2
Ifv2=fftshift(to_visualize_TF(If2));

%Visualisation du spectres normalisés
Ifv_log2=fftshift(to_visualize_TF_log(If2));
Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);
figure,imagesc(Ifv_log2_normalized);

%2c) BONUS Deuxieme experience
Fe=0.5*Fm;

image3 = sinusoid2d(amplitude, theta,taille, TO,1/Fe);
image_normalized3=normaliseImage(image3,0,1);
%Visualisation de l'image échantillonnée
figure,imshow(image_normalized3);
%Calcul de la TF de image3
If3=compute_FT(image3);
%Visualisation du spectres normalisés
Ifv_log3=fftshift(to_visualize_TF_log(If3));
Ifv_log3_normalized=normaliseImage(Ifv_log3,0,1);
figure,imagesc(Ifv_log3_normalized);