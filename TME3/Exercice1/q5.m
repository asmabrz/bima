amplitude=10;
theta=10;
TO=64;
Te=1; 
taille=512;
Fe=(3/2)*Fm;

image6 = sinusoid2d(amplitude, theta,taille, TO,1/Fe);
image_normalized6=normaliseImage(image6,0,1);

%Visualisation de l'image échantillonnée
figure,imshow(image_normalized6);

%Calcul de la TF de image6
If6=compute_FT(image6);

%Visualisation du spectres normalisés
Ifv_log6=fftshift(to_visualize_TF_log(If6));
Ifv_log6_normalized=normaliseImage(Ifv_log6,0,1);
figure,imshow(Ifv_log6_normalized);
figure,imagesc(Ifv_log6_normalized);


%reconstruction de l'image
imager6 = reconstruction(image4,1/Te,size(image6,1));
%Visualisation de Image6 reconstruite


%Calcul de la TF de imager6
Ifr6=compute_FT(imager6);

%Visualisation du spectres normalisés
Ifv_logr6=fftshift(to_visualize_TF_log(Ifr6));
Ifv_logr6_normalized=normaliseImage(Ifv_logr6,0,1);


figure,imshow(Ifv_logr6_normalized);
figure,imagesc(Ifv_logr6_normalized);


eps = erreur(imager6,image6,amplitude)