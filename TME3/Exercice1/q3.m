amplitude=10;
theta=45;
TO=64;
Te=1; 
taille=512;

%2Calcul des fréquences max u et t
Fm=0.85/64;


%Echantillonnage question 3
Fe=4*Fm;

image4 = sinusoid2d(amplitude, theta,taille, TO,1/Fe);
image_normalized4=normaliseImage(image4,0,1);

%Visualisation de l'image échantillonnée
figure,imshow(image_normalized4);

%Calcul de la TF de image4
If4=compute_FT(image4);

%Visualisation du spectres normalisés
Ifv_log4=fftshift(to_visualize_TF_log(If4));
Ifv_log4_normalized=normaliseImage(Ifv_log4,0,1);
figure,imagesc(Ifv_log4_normalized);

%reconstruction de l'image
imager4 = reconstruction(image4,1/Te,size(image4,1));
%Visualisation de Image4 reconstruite


%Calcul de la TF de imager4
Ifr4=compute_FT(imager4);

%Visualisation du spectres normalisés
Ifv_logr4=fftshift(to_visualize_TF_log(Ifr4));
Ifv_logr4_normalized=normaliseImage(Ifv_logr4,0,1);


figure,imshow(Ifv_logr4_normalized);
figure,imagesc(Ifv_logr4_normalized);

%Calcul de l'erreur
eps = erreur(imager4,image4,amplitude)


