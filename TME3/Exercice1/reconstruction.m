function imager = reconstruction(imagee,Te,L)
[ imr ] = shannon_interpolation ( imagee , L , Te);
imager=imr;
end