function [ imr ] = shannon_interpolation_V2 ( ime , taille , Te)
Fe=1/Te;
n=size(ime,1);
m=taille;
imr=zeros(n,m);
for t=1:n
    for u=1:m
        for k=1:n
            for l=1:m
                imr(t,u)=imr(t,u)+ime(k*Te,l*Te)*cardinal_sine(pi*Fe*(t-k*Te))*cardinal_sine(pi*Fe*(u-l*Te));
            end
        end
    end
end
end