function imager = reconstruction2(imagee,Te,L)
[ imr ] = shannon_interpolation_V2 ( imagee , L , Te);
imager=imr;
end