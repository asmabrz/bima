function eps = erreur(xr,xd,A) %real noise xR and imaginary noise xd
eps=0;
L=xr(size(xr,1));
for k=1:L
    for l=1:L
    eps=abs(xr(k,l)-xd(k,l));
    end
end

eps=eps/(2*A*L*L);

end