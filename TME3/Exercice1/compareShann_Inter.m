amplitude=10;
theta=10;
TO=64;
Te=1; 
taille=512;
Fe=(3/2)*Fm;

image6 = sinusoid2d(amplitude, theta,taille, TO,1/Fe);
image_normalized6=normaliseImage(image6,0,1);

%Visualisation de l'image échantillonnée
figure,imshow(image_normalized6);

%Calcul de la TF de image6
If6=compute_FT(image6);

%Visualisation du spectres normalisés
Ifv_log6=fftshift(to_visualize_TF_log(If6));
Ifv_log6_normalized=normaliseImage(Ifv_log6,0,1);
figure,imshow(Ifv_log6_normalized);
figure,imagesc(Ifv_log6_normalized);

tic

%reconstruction de l'image avec la premiere version fournie dans le TP
imager6 = reconstruction(image4,1/Te,size(image6,1));
%Visualisation de Image6 reconstruite
toc

%reconstruction de l'image avec construite à partir de l’´equation (3.10)
imager6 = reconstruction2(image4,1/Te,size(image6,1));
%Visualisation de Image6 reconstruite
toc
