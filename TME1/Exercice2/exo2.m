%Question1
I=[128 128 0 255 ; 128 0 0 255 ; 0 128 0 255 ; 128 128 0 255];
imagesc(I);
h=calculerHisto(I);

figure, plot(h);


%Quetion2

%Distribution uniforme
I=rand(512);
imagesc(I);

%Distribution uniforme sur les entiers
K=randi(512);
figure,imagesc(K);

%Distribution gaussienne
J=round(randn(512));
figure, imagesc(J);
figure,plot(calculerHisto(J));      

IM=128 + 50.*J;
figure, imagesc(IM);
figure,plot(calculerHisto(IM));

%Nous remarquons une certaine distribution autour d'une valeur centrale qui est la moyenne 128. L'écart-type représente la largeur de chaque colonne. L'histogramme est plus régulier formant une courbe, appelée loi normale.

%Functions
function nb=compterPixels(I,k)
nb=size(find(I==k),1);
end

function h = calculerHisto(I)
h=zeros(1,256);
for i=0:255
    h(i+1)=compterPixels(I,i);
end
end
