%Question1

I=ouvrirImage('../Images/pout.tif');
imshow(I);	

%Question2
%Calcul de l'histogramme
h=calculerHisto(I);
figure,plot(h);
title('Histogramme de l image originale');
%Niveau de gris max et min
maxH=find(h==max(h))-1; % Le premier indice de h c'est 1 selon matlab. On retranche 1 pour retrouver le bon indice
minH=find(h==min(h),1)-1; %Idem pour le bon indice pour minH que pour maxH
display(maxH);
display(minH);
% On constate qu'il n'y a pas une bonne distribution de niveaux de gris sur
% l'image

%Question3

function egalisationHistogramme(I,h)
n=size(I,1);
m=size(I,2);
maxH=find(h==max(h))-1;
minH=find(h==min(h),1)-1;
nbPix=n*m;

%Proba
for i=1:256
    h(i)=h(i)/nbPix;
end

%Histogramme cumulé (Densité de probabilité cumulative)
Hc=zeros(1,256);
Hc(1)=h(1);
for i=2:256
    Hc(i)=h(i)+Hc(i-1);
end
for i=1:256
    Hc(i)=floor(Hc(i)*255);
end

%Transformation des niveaux de gris de l'image
Ieq=zeros(n,m);%Nouvelle image
for i=1:n
    for j=1:m
        Ieq(i,j)=Hc(I(i,j));
    end
end
Heq=calculerHisto(Ieq);
figure,plot(Heq);
end
%Nous remarquons qu'après l'égalisation, les pixels occupant tout l'espace de valeurs disponible de la dynamique


%Functions
function [I,n,m]=ouvrirImage(nom)
I=imread(nom);
n=size(I,1);
m=size(I,2);
end

function nb=compterPixels(I,k)
nb=size(find(I==k),1);
end

function h = calculerHisto(I)
eg=zeros(I(1),I(2));
h=zeros(1,256);
for i=0:255
    h(i+1)=compterPixels(I,i);
end
end
