%Question1
function [I,n,m]=ouvrirImage(nom)
I=imread(nom);
n=size(I,1);
m=size(I,2);
end

%Question2
function nb=compterPixels(I,k)
nb=size(find(I==k),1);
end

%Question3
function J=remplacerPixels(I,k1,k2)
J=I;
K=find(J>k1);
J(K)=k2;
end

%Question4
function J=normaliseImage(I,k1,k2)
J=(I-min(I)).*((k2-k1)./(max(I)-min(I)))+k1;
end


%Question5
function J=inversionImage(I)
J=255-I
end

%Question6
function h = calculerHisto(I)
h=zeros(1,256);
for i=0:255
	h(i+1)=compterPixels(I,i);
end
end

%Question7
function I = seuillerImage(I,s)
I2=I;
I2(I>s)=255;
I2(I<=s)=0;
end

%Question8
function q8(nom)
[I,n,m]=ouvrirImage(nom);
imshow(I);
h = calculerHisto(I);
figure, plot(h);
title("Histogramme de l'image originale");
V=inversionImage(I);
hv=calculerHisto(V);
figure, plot(hv);
title("Histogramme de l'image inversée");
end

%Question9
function q9(nom)
[I,n,m]=ouvrirImage(nom);
imshow(I);
h = calculerHisto(I);
figure, plot(h);
title("Histogramme de l'image originale");
N=normaliseImage(I,10,50);
figure,imshow(N);
hn=calculerHisto(N);
figure, plot(hn);
title("Histogramme de l'image normalisée");
end

%Interpretation du résultat des traitements
%Le contraste est trop acccentué: L'histogramme est très étroit sur les niveaux de gris les plus sombres.

%Question10
function q10(nom)
[I,n,m]=ouvrirImage(nom);
imshow(I);
h = calculerHisto(I);
figure, plot(h);
title("Histogramme de l'image originale");
S=seuillerImage(I,128);
figure, imshow(S);
hs=calculerHisto(S);
figure, plot(hs);
title("Histogramme de l'image après seuillage");
end

