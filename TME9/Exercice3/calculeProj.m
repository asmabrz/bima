function z=calculeProj(x,x_moy,K,W)
Wk=W(:,1:K);
WkT=Wk';
size(WkT);
size(x);
z=WkT*(x-x_moy);
