clear all
close all

load('YaleFaces.mat')


K=[5 ,10, 25 ,50 ,90];

%Q5 pour l'image 50
x50=X_train(:,50);
x_moy=meanVisage(X_train);
xc=centrerVisage(X_train,x_moy);
[u,lmb]=eigenfaces(xc);
W=u;
WT=W';

m=1;
figure();
subplot(2, 3, m); colormap(gray);
imagesc(reshape(x50,[64,64]));
title(['Depart']);

for i=1:size(K,2)
    m = m+1 ;
    %Calcul des coocolormap(gray);rdonnées z dans le sous-espace Wk 
    z=calculeProj(x50,x_moy,K(i),W);
    x_r=reconstruction(z,x_moy,W,K(i));
    subplot(2, 3, m);
    affiche_Reconstruction(x_r,x50);
    er = erreur_Reconstruction(x_r,x50);
    title([num2str(K(i)), '  ',num2str(er)]);

end

%Q5 Pour l'image 55
x55=X_train(:,55);

z=calculeProj(x55,x_moy,90,W);
x_r=reconstruction(z,x_moy,W,90);
figure();
colormap(gray);
affiche_Reconstruction(x_r,x55);
erreur = erreur_Reconstruction(x_r,x55);
title([num2str(90), ' ',num2str(erreur)]);

%Q5 Pour l'image 17
x17=X_test(:,17);

z=calculeProj(x17,x_moy,90,W);
x_r=reconstruction(z,x_moy,W,90);
figure();
colormap(gray);
affiche_Reconstruction(x_r,x17);
erreur = erreur_Reconstruction(x_r,x17);
title([num2str(90), ' ',num2str(erreur)]);

%Q7 Evolution de la moyenne de l'erreur de reconstruction des visages

n=size(X_test,2);
erreurTotale=zeros(1,size(X_train,2));

for i=1:size(X_train,2)
    erreurMoyenne=0;
    for j=1:n
        %Calcul des coordonnées z dans le sous-espace Wk 
        z=calculeProj(X_test(:,j),x_moy,i,W);
        x_r=reconstruction(z,x_moy,W,i);
        erreurMoyenne=erreurMoyenne+erreur_Reconstruction(x_r,X_test(:,j));
    end
    erreurTotale(i)=erreurMoyenne/n;
end

figure();
plot(erreurTotale)

