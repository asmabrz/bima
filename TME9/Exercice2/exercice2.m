clear all
close all
load('YaleFaces.mat')

%Calcul des eigenfaces
xmoy=meanVisage(X_train);
xc=centrerVisage(X_train,xmoy);
[u,lmb]=eigenfaces(xc);

%Normalisation des valeurs propres 
lambdas=diag(lmb);

%La somme des lambdas est notre 100%
lambdasNorm=(lambdas./sum(lambdas))*100;

%affichage du visage moyen
imagesc(reshape(xmoy,[64,64]))
%affichage des eigenfaces
for i=1:15
    
    figure,imagesc(reshape(u(:,i),[64,64]))
end

%affichage des valeurs propres
lambdasNorm

%La courbe de la somme cumulée des valeurs propres normalisees
Vc=zeros(1,90);
Vc(1)=lambdasNorm(1)

for i=2:size(lambdasNorm,1)
    Vc(i)=Vc(i-1)+lambdasNorm(i);
end

figure,plot(Vc)

