clear all
close all

load('YaleFaces.mat')

K=30;
x_moy=meanVisage(X_train);
xc=centrerVisage(X_train,x_moy);
[u,lmb]=eigenfaces(xc);
W=u;

%Calcul del’erreur de reconstruction des images de la base d'entrainement
N=size(X_train,2);
erreurMoyBase=0
erreurBase=zeros(1,N);
for i=1:N
    %Calcul des coordonnées z dans le sous-espace Wk 
    z=calculeProj(X_train(:,i),x_moy,K,W);
    x_r=reconstruction(z,x_moy,W,K);
    erreurBase(i)=erreur_Reconstruction(x_r,X_train(:,i));
    erreurMoyBase=erreurMoyBase+erreurBase(i);
end
erreurMoyBase=erreurMoyBase/N;


%Calcul del’erreur de reconstruction des images de la base de test
M=size(X_test,2);
erreurTest=zeros(1,M);
erreurMoyTest=0
for i=1:M
    %Calcul des coordonnées z dans le sous-espace Wk 
    z=calculeProj(X_test(:,i),x_moy,K,W);
    x_r=reconstruction(z,x_moy,W,K);
    erreurTest(i)=erreur_Reconstruction(x_r,X_test(:,i));
    erreurMoyTest=erreurMoyTest+erreurBase(i);
end

F=size(X_noface,2);
erreurNoFace=zeros(1,F);

for i=1:F
    %Calcul des coordonnées z dans le sous-espace Wk 
    z=calculeProj(X_noface(:,i),x_moy,K,W);
    x_r=reconstruction(z,x_moy,W,K);
    erreurNoFace(i)=erreur_Reconstruction(x_r,X_noface(:,i));
end


display('Test:');
erreurMoyBase=erreurMoyBase/N;

plot(sort(erreurBase)) %bleue
hold on
plot(sort(erreurTest))%rouge
hold off
legend({"Base d'apprentissage",'Base de test'},'Location','northeast')
display('Erreur moyenne de la reconstruction:');
display('Base:');
erreurMoyBase
display('Test:');
erreurMoyTest

display('Erreur min pour les non-visage:');
min(erreurNoFace)


display('Erreur min pour les visage:');
max(erreurTest)

%Q2 Visualisation de l'erreur
%Image de visages

for i=1:10
    m=1;
    figure();
    colormap(gray);
    subplot(1, 2, m);
    imagesc(reshape(X_test(:,i),[64,64]));
    title('Originale');
    m=m+1;
    %Calcul des coocolormap(gray);rdonnées z dans le sous-espace Wk 
    z=calculeProj(X_test(:,i),x_moy,K,W);
    x_r=reconstruction(z,x_moy,W,K);
    subplot(1, 2, m);
    affiche_Reconstruction(x_r,X_test(:,i));
    er = erreur_Reconstruction(x_r,X_test(:,i));
    title([num2str(i), '  ',num2str(er)]);
end

%Image de non-visages

for i=1:10
    m=1;
    figure();
    colormap(gray);
    subplot(1, 2, m);
    imagesc(reshape(X_noface(:,i),[64,64]));
    title('Originale');
    m=m+1;
    %Calcul des coocolormap(gray);rdonnées z dans le sous-espace Wk 
    z=calculeProj(X_noface(:,i),x_moy,K,W);
    x_r=reconstruction(z,x_moy,W,K);
    subplot(1, 2, m);
    affiche_Reconstruction(x_r,X_noface(:,i));
    er = erreur_Reconstruction(x_r,X_noface(:,i));
    title([num2str(i), '  ',num2str(er)]);
end


