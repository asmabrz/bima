function[u ,lambdas]=eigenfaces(Xc)
[u,s,v]=svd(Xc,0);
lambdas=s.^2;
