clear all
close all
load('YaleFaces.mat')

x_moy=meanVisage(X_train);
Xc_train=centrerVisage(X_train,x_moy);
Xc_test=centrerVisage(X_test,x_moy);
[u,lmb]=eigenfaces(Xc_train);
W=u;
K=30;
n=size(Xc_train,2);


D=calculMatDist(Xc_train,Xc_train,W,K);
colormap(gray)
imagesc(D)