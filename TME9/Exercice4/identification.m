function id_test_hat = identification(id_train,D)
id_test_hat=zeros(1,size(D,1));
for i=1:size(D,1)
    [a,tmp]=min(D(i,:));
    id_test_hat(i)=id_train(tmp) ;
end