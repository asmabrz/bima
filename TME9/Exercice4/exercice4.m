clear all
close all
load('YaleFaces.mat')

x_moy=meanVisage(X_train);
Xc_train=centrerVisage(X_train,x_moy);
Xc_test=centrerVisage(X_test,x_moy);
[u,lmb]=eigenfaces(Xc_train);
W=u;

%Q4 Pour K=30 
sum=0;
K=30;
D=calculMatDist(Xc_train,Xc_test,W,K);
id_test_hat=identification(id_train,D);
for i=1:size(id_test,2)
        if id_test(i) == id_test_hat(i)
           sum=sum+1;
        end
end
display('Le taux d identification est')
sum/(size(id_test,2))
%Q4 Nous allons varier K et puis nous allons tracer la courbe fu nombre de
%visages reconnus en fonction de K
evolution_moyenne=zeros(1,size(X_train,2));
for K=1:size(X_train,2)  
    D=calculMatDist(Xc_train,Xc_test,W,K);
    id_test_hat=identification(id_train,D);
    sum=0;
    for i=1:size(id_test,2)
        if id_test(i) == id_test_hat(i)
           sum=sum+1;
        end
    end
    evolution_moyenne(K)=sum;

end
plot(evolution_moyenne);

