function D = calculMatDist(Xc_train,Xc_test,W,K)
Wk=W(:,1:K);
WkT=Wk';
Ntrain=size(Xc_train,2);
Ntest=size(Xc_test,2);
D=zeros(Ntest,Ntrain);

ztest=WkT*Xc_test;
ztrain=WkT*Xc_train;

for i=1:Ntest
    for j=1:Ntrain
        tmp=(ztrain(:,j)-ztest(:,i)).^2;   
        D(i,j)=sqrt(sum(tmp));
    end
    
end