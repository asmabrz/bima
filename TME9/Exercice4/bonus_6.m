clear all
close all
load('YaleFaces.mat')

x_moy=meanVisage(X_train);
Xc_train=centrerVisage(X_train,x_moy);
Xc_test=centrerVisage(X_test,x_moy);
[u,lmb]=eigenfaces(Xc_train);
W=u;
K=30;
n=size(Xc_train,2);


D=calculMatDist(Xc_train,Xc_train,W,K);
n = size(D,1);
D(1:(n+1):end) = NaN;

minmax=zeros(n,2);
for i=1:n
    minmax(i,1)=min(D(i,:));
    minmax(i,2)=max(D(i,:));
end

%Plot des min 
plot(minmax(:,1));

figure,plot(sort(minmax(:,1)));
seuil=1500;

x=Xc_test(:,2) %visage inconnu
Dx=calculMatDist(Xc_train,x,W,K);
minX=min(Dx,
figure,plot(Dx)


