clear all
%Ouverture de l'image 
[J,n,m]=ouvrirImage('../Images/barbara.png');

%transformé de Fourier de l’image après filtrage
If2=compute_FT(J);

%Visualisation du spectres normalisés
Ifv_log2=fftshift(to_visualize_TF_log(If2));
Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);

%Sous-́echantillonnage de l'image récursive
while(~isnan(Ifv_log2_normalized))
	[ J ] = subSampling2( J );
	%Visualisation de l'image
	figure, imshow(J/255);
	%transformé de Fourier de l’image initiale 
	If2=compute_FT(J);
	%Visualisation du spectres normalisés
	Ifv_log2=fftshift(to_visualize_TF_log(If2));
	Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);   
    figure,imagesc(Ifv_log2_normalized);
end
