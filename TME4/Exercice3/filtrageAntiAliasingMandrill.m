%Ouverture de l'image 
[I,n,m]=ouvrirImage('../Images/mandrill.png');
 
%Visualisation de l'image initiale
figure, imshow(I);

%transformé de Fourier de l’image initiale 
If1=compute_FT(I);

%Visualisation du spectres normalisés
Ifv_log1=fftshift(to_visualize_TF_log(If1));
Ifv_log1_normalized=normaliseImage(Ifv_log1,0,1);

[F]= antiAliasingFilter(n,m);

%Calcul de la FT
If=fftshift(compute_FT(I));
Ifiltred=If.*F; % point -> terme à terme
Itf=abs(ifft2(ifftshift(Ifiltred)));
figure,imshow(normaliseImage(Itf,0,1));

J=Itf;

%transformé de Fourier de l’image après filtrage
If2=compute_FT(J);

%Visualisation du spectres normalisés
Ifv_log2=fftshift(to_visualize_TF_log(If2));
Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);

%Sous-́echantillonnage de l'image récursive
while(~isnan(Ifv_log2_normalized))
	[ J ] = subSampling2( J );
	%Visualisation de l'image
	figure, imshow(J/255);
	%transformé de Fourier de l’image initiale 
	If2=compute_FT(J);
	%Visualisation du spectres normalisés
	Ifv_log2=fftshift(to_visualize_TF_log(If2));
	Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);   
    figure,imagesc(Ifv_log2_normalized);
end

