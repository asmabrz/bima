function [J]= imagePad(I,h)
d=size(h,1);
J=padarray(I,[d-1,d-1],0);
end