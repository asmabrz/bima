%Ouverture de l'image 
[I,n,m]=ouvrirImage('../Images/barbara.png');
 
%Visualisation de l'image initiale

[F]= antiAliasingFilter(n,m);

%Calcul de la FT
If=fftshift(compute_FT(I));
Ifiltred=If.*F; % point -> terme à terme
Itf=abs(ifft2(ifftshift(Ifiltred)));
figure,imshow(normaliseImage(Itf,0,1));


J=Itf1;

%transformé de Fourier de l’image initiale 
If2=compute_FT(J);

%Visualisation du spectres normalisés
Ifv_log2=fftshift(to_visualize_TF_log(If2));
Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);

k=0;
%Sous-́echantillonnage de l'image récursive
while(~isnan(Ifv_log2_normalized))
	[ J ] = subSampling2( J );

    
    display(k)
    k=k+1;
	%Visualisation de l'image
	figure, imshow(J/255);

	%transformé de Fourier de l’image initiale 
	If2=compute_FT(J);

	%Visualisation du spectres normalisés
	Ifv_log2=fftshift(to_visualize_TF_log(If2));
	Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);
    
	figure,imagesc(Ifv_log2_normalized);

end

