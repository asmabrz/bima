clear all

%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

%Filtre moyenneur 7x7
h7 = ones(7)/49;

%Zero-padding 
Ip3=imagePad(I1,h7);

%Filtrage
If3=convolution(Ip3,h7);

figure,mesh(If3);
If3_norm=normaliseImage(If3,0,1);
figure,imshow(If3_norm);
