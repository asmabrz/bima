%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

%Filtre moyenneur 3x3
h3 = ones(3)/9;

J=sameSize(h3,I1);

If=compute_FT(J);
Ifv_log1=fftshift(to_visualize_TF(If));
mesh(Ifv_log1);
