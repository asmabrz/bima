clear all
%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

%Filtre moyenneur 3x3
h3 = ones(3)/9;
[n2,m2]=size(h3);

%Zero-padding 
Ip1=imagePad(I1,h3);

%Filtrage
If1=convolution(Ip1,h3);
figure,mesh(If1);
If1_norm=normaliseImage(If1,0,1);
figure,imshow(If1_norm);


