clear all

%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

sigma=0.5; %3sigma=3x0.5=1.5~2 => filtre de taille 5x5
h = gauss( sigma ) ;

%Zero-padding 
Ip1=imagePad(I1,h);

%Filtrage
If1=convolution(Ip1,h);
figure,mesh(If1);
If1_norm=normaliseImage(If1,0,1);
figure,imshow(If1_norm);
