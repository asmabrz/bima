clear all

%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

sigma=1/3; %3sigma=3x1/3=1 => filtre de taille 3x3
h = gauss( sigma ) ;

size(h)
%Zero-padding 
Ip1=imagePad(I1,h);

%Filtrage
If1=convolution(Ip1,h);
figure,mesh(If1);
If1_norm=normaliseImage(If1,0,1);
figure,imshow(If1_norm);
