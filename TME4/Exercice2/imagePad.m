function [J]= imagePad(I,h)
[n,m]=size(h);
J=padarray(I,[n-1,m-1],0);
end