%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

%Filtre moyenneur 5x5
h5 = ones(5)/25;

%Zero-padding 
Ip2=imagePad(I1,h5);

%Filtrage
If2=convolution(Ip2,h5);
figure,mesh(If2);
If2_norm=normaliseImage(If2,0,1);
figure,imshow(If2_norm);
