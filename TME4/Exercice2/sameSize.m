function [r]=sameSize(b,a)
n1=size(a,1);
m1=size(a,2);

n2=size(b,1);
m2=size(b,2);

%in case one of the matrix is odd, we have to subtract 1 before devision
%then add it at the end at the beginning or end of columns/lines


i=0;
j=0;

if ((mod(n1, 2) == 0) && (mod(n2, 2) ~= 0)) || (mod(n2, 2) == 0) && (mod(n1, 2) ~= 0)
  % n1 is even, n2 is odd OR n2 is even, n1 is odd
  i=1; %To precize that we have already substract 1, to add a line of zeros at the end of process
  n=n1-n2-1; %for the result to be even
  n=n/2; %Number of lines to add in top and bottom
  
else
    n=(n1-n2)/2;
end
%Same process for columns
if ((mod(m1, 2) == 0) && (mod(m2, 2) ~= 0)) || (mod(m2, 2) == 0) && (mod(m1, 2) ~= 0)
  % m1 is even, m2 is odd OR m2 is even, m1 is odd
  j=1; %To precize that we have already substract 1, to add a column of zeros at the end of process
  m=m1-m2-1; %for the result to be even
  m=m/2; %Number of lines to add in top and bottom
  
else
    m=(m1-m2)/2;
end

%Lets padding-zeros
r=padarray(b,[n,m],0);
%Now we should complete with zeros if we had even result
if i==1
    l=zeros(1,size(r,2));
    r=vertcat(l,r); %just a choice to add top, can be at the end
end
if j==1
c=zeros(1,size(r,1));
    r=horzcat(r,transpose(c)); %just a choice to add right, can be left
end
end