function [If]=convolution(f,h)

[n1,m1] = size(f);
[n2,m2] = size(h);


n=n1+2*(n2-1);
m=m1+2*(m2-1);
F = zeros(n,m);

F(n2:(end-n2+1),m2:(end-m2+1)) = f;

H = rot90(h,2);
HH = H(:);
If = zeros(n2+n1-1,m1+m2-1);

for i = 1:n-n2+1
    for j =  1:m-m2+1
        x = F(i:i+n2-1,j:j+m2-1);
        If(i,j) = x(:)'*HH;
    end
end
end

