%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

%Filtre moyenneur 7x7
h7 = ones(7)/49;
[n2,m2]=size(h7);

J=sameSize(h7,I1);

If=compute_FT(J);
Ifv_log1=fftshift(to_visualize_TF(If));
mesh(Ifv_log1);

