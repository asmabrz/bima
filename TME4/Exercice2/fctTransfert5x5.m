%Ouverture de l'image
[I1,n1,m1]=ouvrirImage('../Images/mandrill.png');

%Filtre moyenneur 5x5
h5 = ones(5)/25;
[n2,m2]=size(h5);

J=sameSize(h5,I1);

If=compute_FT(J);
Ifv_log1=fftshift(to_visualize_TF(If));
mesh(Ifv_log1);
