%Ouverture de l'image
[I,n,m]=ouvrirImage('../Images/lena.jpg');
%Calcul de la FT
If1=compute_FT(I);
Ifv1=fftshift(to_visualize_TF(If1));

%Calcul de Fc
Fc=Ifv1(floor(n/300),floor(m/300))

%Calcul du filtre passe-bas
[Ff] =filtrePasseBasIdeal(n,m,Fc);

%Filtrage de la TF
If1Filtred=fftshift(If1).*Ff;

%Inversion de la shit de la FFT filtrée, calcul de sa TF inverse
Itf1=ifft2(ifftshift(If1Filtred));

%Visualisation des spectres
Ifv_log1Fil_normalized=fftshift(to_visualize_TF_log(compute_FT(Itf1)));
figure,imagesc(Ifv_log1Fil_normalized);

figure,imshow(normaliseImage(real(Itf1),0,1));
