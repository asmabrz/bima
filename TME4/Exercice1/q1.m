%Ouverture de l'image
[I,n,m]=ouvrirImage('../Images/mandrill.png');
%Calcul de la FT
If1=compute_FT(I);
Ifv1=fftshift(to_visualize_TF(If1));

%Visualisation deu module centré de la TF
Ifv_log1=fftshift(to_visualize_TF_log(If1));
figure,imagesc(Ifv_log1);