function [iv]=quantification(v,k)
% renvoie l'appartenance de v � l'intervalle d�coupe en  K  
% prend en entree un scalaire et non une matrice
% renvoie un scalaire et non une matrice
iv=0;

% ou mettre && � la place de &

if ((v >= 0) & (v < 1))
    iv=floor(v*k)+1;
    
elseif (v==1)
    iv= k;

end
end 