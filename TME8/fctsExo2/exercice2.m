% Variables needed
load('./histos/ListHisto.mat', 'Listhist');
% load('./ListHist.mat');
sim = calculSimilarities(Listhist);
indexQuery = 319;
indexQuery = 591;
k = 20;

% Matrix with images' name
pathImage = './Base/';
listImage = dir(pathImage); 
nameImageBase = [ ];
    for i=1:length(listImage)
        if(listImage(i).isdir == 0)
            fileName = listImage(i).name;
            nameImageBase = strvcat(nameImageBase,strcat(pathImage, fileName));
        end
    end

% Searching for most similar images
[valeurs,indices] = sort(sim(indexQuery,:),'descend'); 

% Visualizing most similar images
figure();
for i=1:k
    subplot(5,4,i);
    imagesc(imread(nameImageBase(indices(i),:)));
end

