% Placed in TME8_ex2 or change path
pathImage = './Base/';
nH = 12;nS = 3;nV = 8;
% lists the files in a folder
listImage = dir(pathImage); 
counter = 1;

% Calculate HSV for each image
for i=1:length(listImage)
    % There are files (not image) with isdir = 1
    if listImage(i).isdir == 0 
        fileName = listImage(i).name;
        % Calculate histogram
        I = imread([pathImage,fileName]);
        I = double(I);
        I = I./65535;
        J = rgb2hsv(I);  
        [Iq , histo] = quantificationImage(J,nH,nS,nV);
        histo = histo(:);    
        histo = normalise(histo); 
        % adding in listHist
        listHist(counter,:) = histo;
        counter = counter + 1; 
    end
end

% saving in current path by the name 'ListHist.mat'
nomList = strcat('./','ListHist.mat');
save(nomList,'listHist');
