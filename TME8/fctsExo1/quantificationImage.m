function [Iq,histo]=quantificationImage(I, nH,nS,nV)
% [n,m]=size(I);

n=size(I,1);
m=size(I,2);

I = double(I);

Ih=I(:,:,1);
Is=I(:,:,2);
Iv=I(:,:,3);
    
Ihr = Ih;
Isr = Is;
Ivr = Iv;

% Histo 3 D où les index sont le resultat de la fonction quantification par composante HSV

histo(nH,nS,nV)=0;
Iq(n,m,3)=0;

for o = 1:n
    for p= 1:m
        histo(quantification(Ihr(o,p),nH),quantification(Isr(o,p),nS), quantification(Ivr(o,p),nV))= histo(quantification(Ihr(o,p),nH),quantification(Isr(o,p),nS), quantification(Ivr(o,p),nV))+1;
        Iq(o,p,1)=uint8(quantification(Ihr(o,p),nH));
        Iq(o,p,2)=uint8(quantification(Isr(o,p),nS)); 
        Iq(o,p,3)=uint8(quantification(Ivr(o,p),nV));
        
    end
end
  
end