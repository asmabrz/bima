function [histon] = normalise(histo)
%NORMALISE Summary of this function goes here
%   Detailed explanation goes here


% sum((sum(sum(histo.*histo)))).^0.5;

% eviter de faire le produit scalaire car il y a un risque de creer une matrice

l2 = (sum(histo.*histo)).^0.5;

histon = histo./l2;

end

