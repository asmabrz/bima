clear all;
close all;


%%%%%%%%%% A Completer %%%%%%%%%% 
PathImage = './';

nom = 'Paysages67.png';
nom = 'Aeriennes10.png';
nom = 'Liontigre1.png';
nom = 'Fleche16.png';
nom = 'Fleche23.png';
nom = 'Bestioles69.png';
nom = 'Fleche23.png';
nom = 'Paysages67.png';


% Quantif HSV : Completer avec votre nombre de bins
nH = 12;
nS = 3;
nV = 8;

% Quantif HSV : Completer avec votre nombre de bins
% nH = 6;
% nS = 16;
nV = 5;


filename= nom;
I=imread([PathImage,filename]);
I=double(I);
I = I ./ 65535;

figure();
imagesc(I);

% conversion RGB->HSV
J = rgb2hsv(I);

[ palette,palette2 ] = calculPalette( nH, nS , nV );

%%%%%%%%%% COMPL�TER AVEC VOTRE FONCTION DE CALCUL D'HISTOGRAMME HSV %%%%%%%%%% 
[Iq , histo3d] = quantificationImage(J,nH,nS,nV);

% Visualisation de l'image quantifi�e
visuQuantification( Iq , palette2);

% transformation de l'histogramme en 1 vecteur 1D
histo1d = histo3d(:);
% COMPL�TER AVEC VOTRE FONCTION DE NORMALISATION D'HISTOGRAMME
histo = normalise(histo1d);
 
figure();
plot(histo);

affiche5dominantes( histo , palette )
[Y,Ind] = sort(histo,'descend');



