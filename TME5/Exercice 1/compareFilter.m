clear all
[I,n,m]=ouvrirImage('../Images/lena.gif');
[A]=edgeSobel(I);
[B]=edgeLeplacian(I);
subplot(1,2,1);
imshow(A);
subplot(1,2,2);
imshow(B);