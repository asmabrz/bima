function [B]=edgeSobel(I)

Gx=[1 0 -1;2 0 -2;1 0 -1];
Gy=[1 2 1;0 0 0;-1 -2 -1];

%a) Calcul des approximations des dérivées horizontale et verticale
Ix=convolution(I,Gx);
Iy=convolution(I,Gy);
%Ix=conv2(I,Gx,'same');
%Iy=conv2(I,Gy,'same');


%b) Module du gradient
G=sqrt(Ix.*Ix+Iy.*Iy);

theta=atan2(Iy,Ix);

%c) seuillage
s=70;
B = uint8((G>s)*255);

end
