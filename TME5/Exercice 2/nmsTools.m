close all
[I,n,m]=ouvrirImage('../Images/tools.gif');

Gx=[1 0 -1;2 0 -2;1 0 -1];
Gy=[1 2 1;0 0 0;-1 -2 -1];

Ix=convolution(I,Gx);
Iy=convolution(I,Gy);
G=sqrt(Ix.*Ix+Iy.*Iy);
Or= orientation(Ix,Iy ,G);
Img=nms(G,Or);

imshow(normaliseImage(Img,0,1));
