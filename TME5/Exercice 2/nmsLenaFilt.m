close all
[I,n,m]=ouvrirImage('../Images/lena.gif');
sigma=1/3; 
h = gauss( sigma ) ;
Ih=convolution(I,h);
Gx=[1 0 -1;2 0 -2;1 0 -1];
Gy=[1 2 1;0 0 0;-1 -2 -1];

Ix=convolution(Ih,Gx);
Iy=convolution(Ih,Gy);
G=sqrt(Ix.*Ix+Iy.*Iy);
Or= orientation(Ix,Iy ,G);
Img=nms(G,Or);

imshow(normaliseImage(Img,0,1));
