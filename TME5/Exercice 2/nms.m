function [Img]=nms(Ig,Ior)
Img=zeros(size(Ig));
for i=2:size(Ig,1)-1
    for j=2:size(Ig,2)-1
        theta=Ior(i,j);
        switch theta
            case 1 %Rouge
                if (Ig(i-1,j)< Ig(i,j)) && (Ig(i+1,j)< Ig(i,j))

                    Img(i,j)=Ig(i,j);
                end
            case 2 %Vert
                if (Ig(i-1,j+1)< Ig(i,j)) && (Ig(i+1,j-1)< Ig(i,j))
                    Img(i,j)=Ig(i,j);
                end
            case 3 %Bleu
                if (Ig(i-1,j)< Ig(i,j)) && (Ig(i+1,j)< Ig(i,j))

                    Img(i,j)=Ig(i,j);
                end
            case 4 %jaune
                if (Ig(i-1,j-1)< Ig(i,j)) && (Ig(i+1,j+1)< Ig(i,j))
                    Img(i,j)=Ig(i,j);
                end
            otherwise
        end
    end
end