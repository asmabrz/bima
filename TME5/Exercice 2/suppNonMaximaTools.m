close all
[I,n,m]=ouvrirImage('../Images/tools.gif');

Gx=[1 0 -1;2 0 -2;1 0 -1];
Gy=[1 2 1;0 0 0;-1 -2 -1];

Ix=convolution(I,Gx);
Iy=convolution(I,Gy);
G=sqrt(Ix.*Ix+Iy.*Iy);
Or= orientation(Ix,Iy ,G);
myColorMap=[
    0 0 0;
    1 0 0;
    0 1 0;
    0 0 1;
    1 1 0
   ]
imshow(normaliseImage(Or,0,1));
colormap(myColorMap);
colorbar;
