function lisseGaussDec(I,sigma)

h= gauss(sigma) ;
%Lissage gaussien
Ig=convolution(I,h);
figure,imshow(normaliseImage(Ig,0,1));

%Detection de contour avec le filtre de Sobel
I1=edgeSobel(Ig);
figure,imshow(I1);

%Detection de contour avec le filtre de Laplacien
I2=edgeLeplacian(Ig);
figure,imshow(I2);
end