function [B]=edgeLeplacian(I)
%1)
L=[0 1 0;1 -4 1;0 1 0];

Il=convolution(I,L);

[n,m]=size(Il);
s=10;
B=ones(n,m)*255;
%2)
%elemination des termes du bord
for i=2:n-1
    for j=2:m-1
        maxIl=max([Il(i-1,j-1),Il(i-1,j),Il(i-1,j+1),Il(i,j-1),Il(i,j),Il(i,j+1),Il(i+1,j-1),Il(i+1,j),Il(i+1,j+1)]);
        minIl=min([Il(i-1,j-1),Il(i-1,j),Il(i-1,j+1),Il(i,j-1),Il(i,j),Il(i,j+1),Il(i+1,j-1),Il(i+1,j),Il(i+1,j+1)]);
        if ((maxIl>0) && (minIl<0) && (maxIl-minIl<s))
            B(i,j)=0;
        end
    end
end

end