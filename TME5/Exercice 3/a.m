
%declaration filtre gaussien
filtre_gauss_4 = gauss(4);
filtre_gauss_3 = gauss(3); % filtre 19 x 19 
filtre_gauss_2 = gauss(2); % filtre 13 x 13 
filtre_gauss_1 = gauss(1); % filtre 7 x 7 


close all

[m,n,mm]=ouvrirImage('../Images/lena.gif');
% subplot(2,2,1) 
% figure(1),imshow(m,[0 255]);



%Selection du filtre avec sigma faible
filtre = filtre_gauss_1 ; 

Img = m;

for ite=1:6
    % 1 - application et affichage pour le filtre gaussien
    Img_filtree=convolution(Img,filtre);
%     figure(),imshow(Img_filtree,[0 255]);
    
    Img=subSampling2(Img_filtree);
    
    [A200]=edgeSobel(Img);
    figure(),subplot(1,2,1),imshow(A200)
    
    [B10]=edgeLeplacian(Img);
    subplot(1,2,2), imshow(B10);

    
   
end


%Selection du filtre avec sigma haut
filtre = filtre_gauss_4 ; 

Img = m;

for ite=1:6
    % 1 - application et affichage pour le filtre gaussien
    Img_filtree=convolution(Img,filtre);
%     figure(),imshow(Img_filtree,[0 255]);
    
    Img=subSampling2(Img_filtree);
    
    [A200]=edgeSobel(Img);
    figure(),subplot(1,2,1),imshow(A200)
    
    [B10]=edgeLeplacian(Img);
    subplot(1,2,2), imshow(B10);

    
   
end