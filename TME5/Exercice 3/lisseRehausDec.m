%Ouverture de l'image
[I,n,m]=ouvrirImage('../Images/lena.gif');
imshow(I);
%Filtre rehausseur de contraste
fH=[1 -3 1;-3 9 -3;1 -3 1];

Ig=convolution(I,fH);
figure,imshow(normaliseImage(Ig,0,1));

%Detection de contour avec le filtre de Sobel
I1=edgeSobel(Ig);
figure,imshow(I1);

%Detection de contour avec le filtre de Laplacien
I2=edgeLeplacian(Ig);
figure,imshow(I2);
