clear all
%Ouverture de l'image 
[Ig,n,m]=ouvrirImage('../Images/lena.gif');
sigma=3.82;
h= gauss(sigma) ;

%Construction de la pyramide
for i=1:6
    %lissage gaussien
    Ig=convolution(Ig,h);
	[Ig] = subSampling2(Ig);
	%Visualisation de l'image
	figure, imshow(Ig/255);
    
    %Detection de contour avec le filtre de Sobel
    I1=edgeSobel(Ig);
    figure,imshow(I1);

    %Detection de contour avec le filtre de Laplacien
    I2=edgeLeplacian(Ig);
    figure,imshow(I2);
end
