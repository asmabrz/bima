    [I,n,m]=ouvrirImage('../Images/house2.jpg');
    echelle=15
    

%function [R]=calcylR(I,echelle)
    k = 0.04;
    %Masque Gradient
    gx1 = [0 1 0]; 
    gx2 = [1 0 -1];
    
    gy1=gx2;
    gy2=gx1;
    %Masque gaussien 1d
    w=gauss1d(echelle);
    
    Ix = convolution_separable(I, gx1,gx2);    
    Iy=convolution_separable(I, gy1,gy2); 

    Ix2=Ix.^2;
    Iy2=Iy.^2;
    IxIy=Ix.*Iy;
    
   
    Ix2w = conv2(Ix2, w,'same');  
    Iy2w = conv2(Iy2, w,'same');
    IxIyw = conv2(IxIy,w,'same');
    
    M=Ix2w.*Iy2w - IxIyw.^2;
    d=det(M);
    t=trace(M)
    
    R=d-conv2(k,t.^2,'same')
    
    figure,display(R)

%end