function [Rnms]=nms(R,Rb)
Rnms=Rb
for i=2:size(Rnms,1)-1
    for j=2:size(Rnms,2)-1
        if R(i,j)~=max([R(i-1,j-1),R(i-1,j),R(i-1,j+1),R(i,j-1),R(i,j),R(i,j+1),R(i+1,j-1),R(i+1,j),R(i+1,j+1)])
            Rnms(i,j)=0;
        end
    end
end
end