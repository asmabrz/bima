function [R]=calculR(I,echelle)
k=0.04
Gx= [0 0 0;1 0 -1; 0 0 0];
Gy=Gx';

    
gx2 = [1 0 -1]; 
gx1 = [0 1 0]';

% gy2 = [0 1 0]'; 
% gy1 = [1 0 -1];

%Masque gaussien 1d uniquement (et non en 2D)
w=gauss1d(echelle);

Ix=convolution_separable(I, gx2, gx1);
Iy=convolution_separable(I, gx1',gx2');


%Ix=conv2(I,Gx,'same');
%Iy=conv2(I,Gy,'same');
Ix2=Ix.^2;
Iy2=Iy.^2;
IxIy=Ix.*Iy;


Ix2w=convolution_separable(Ix2, w', w);
Iy2w=convolution_separable(Iy2, w', w);
IxIyw = convolution_separable(IxIy,w', w);

% M=[Ix2w IxIyw;IxIyw Iy2w ];
d=Ix2w.*Iy2w - IxIyw.^2;
% d=det(M);
t=Ix2w + Iy2w;

R=d-k.*t.^2;

end
