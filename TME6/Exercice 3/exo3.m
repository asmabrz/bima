clear all
close all
load('toyHorse1.mat');
load('toyHorse2.mat');
figure,imagesc(I1)

figure,imagesc(I2)
echelle=15;

R1=calculR(I1,echelle);
R2=calculR(I2,echelle);

Rb1= seuilleR(R1,800000);
Rb2= seuilleR(R2,100);

%figure,imagesc(R1);
%figure,imagesc(R2);

figure,imagesc(Rb1);
figure,imagesc(Rb2);

a1=nms(R1,Rb1);
a2=nms(R2,Rb2);

figure,imagesc(a1);
figure,imagesc(a2);

dynamique1=max(I1(:))-min(I1(:))+1
dynamique2=max(I1(:))-min(I2(:))+1
