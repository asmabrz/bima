function I3 = blend(I1,I2,alpha)
I1=double(I1);
I2=double(I2);
n=size(I1,1);
m=size(I2,2);
I3=zeros(n,m);
for t=1:n
    for u=1:m
        I3(t,u)=alpha*I1(t,u)+(1-alpha)*I2(t,u);
    end
end

end