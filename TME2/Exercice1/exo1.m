%Spécification des paths
image1=('../Images/son.gif');
image2=('../Images/sonrot.gif');

%Ouvertures des 2 images
[I1,n1,m1]=ouvrirImage(image1);
[I2,n2,m2]=ouvrirImage(image2);

I1=double(I1); %For double precision
I2=double(I2); %For double precision

%Calcul de la TF des 2 images
If1=compute_FT(I1);
If2=compute_FT(I2);

%Calcul du spectre
Ifv1=fftshift(to_visualize_TF(If1));
Ifv2=fftshift(to_visualize_TF(If2));

%Calcul du spectre_log
Ifv_log1=fftshift(to_visualize_TF_log(If1));
Ifv_log2=fftshift(to_visualize_TF_log(If2));

%Normalisation des données
Ifv_log1_normalized=normaliseImage(Ifv_log1,0,1);
Ifv_log2_normalized=normaliseImage(Ifv_log2,0,1);

%Visualisation des spectres log
figure,imshow(Ifv_log1_normalized);
figure,imshow(Ifv_log2_normalized);

%Binaristion des spectres avec le seuil 3.10⁵
J1=image_binarization(Ifv1,3e5);
J2=image_binarization(Ifv2,3e5);


%Visualisation des spectres seuillés
figure,imshow(J1/255); % the values must be between 0 and 1
figure,imshow(J2/255);

% Question 4
% Interprétation des résultats:
%Après une applique du seuillage (3 * 10^5), les pics du spectre apparaîtront beaucoup plus clairement 

%La forme des spectres illustre:
%Pour la figure 'son.gif': L'image possède des lignes horizontales que l'on
%retruove sur la transoformée
%Pour la figure 'sonrot.gif': L'image possède des lignes diagonales que
%l'on retrouve sur la trnasformée

%Question5

%Appel de la fonction blend
I3 = blend(I1,I2,0.5);
I3=double(I3);

%figure,imshow(I3);
%Calcul de la TF de l'image résultante
If3=compute_FT(I3);

%Calcul du spectre
Ifv3=fftshift(to_visualize_TF(If3));

%Calcul du spectre_log
Ifv_log3=fftshift(to_visualize_TF_log(If3));

%Normalisation des données
Ifv_log3_normalized=normaliseImage(Ifv_log3,0,1);

%La valeur max du spectre
max_Ifv3=max(Ifv3(:));

%La valeur min du spectre
min_Ifv3=min(Ifv3(:));

%Calcul de l'histogramme pour trouver le seuil
h=histogram(Ifv_log3_normalized);

%Capturer le seuil conrrespondant à la valeur la plus elevée dans l'histo
threshold_normalized=(find(h.Values==max(h.Values(:))))/(size(h.Values,2));

%Dénormalisation de la valeur
threshold=denormaliseImage(threshold_normalized,0,255,min_Ifv3,max_Ifv3);

%Binaristion des spectres seuillées avec le seuil trouvé
J3=image_binarization(Ifv3,threshold);

%Visualisation des spectres seuillés
figure,imshow(J3/255); 

%Question6
%%Le  module  seuillé  de  l’image  combinée  est la superposition des modules seuillés des  deux  images
%initiales
%Proproété: la linéarité



