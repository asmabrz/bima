function I2 = image_binarization(I,s)
I2=I;
I2(I>s)=255;
I2(I<=s)=0;
end