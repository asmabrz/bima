function I=denormaliseImage(J,k1,k2,minI,maxI)
I=-k1+minI+(maxI-minI).*(J-k1)./(k2-k1);
end
