function [Ir,ordom] = rectify_orientation(I)
I=double(I);

%(a)
%Calcul de la TF de I
If=compute_FT(I);

%Calcul du spectre
Ifv=fftshift(to_visualize_TF(If));

%Calcul du spectre_log
Ifv_log=fftshift(to_visualize_TF_log(If));

%Normalisation des données
Ifv_log_normalized=normaliseImage(Ifv_log,0,1);

%La valeur max du spectre
max_Ifv=max(Ifv(:));

%La valeur min du spectre
min_Ifv=min(Ifv(:));

%Calcul de l'histogramme pour trouver le seuil
h=histogram(Ifv_log_normalized);

%Capturer le seuil conrrespondant à la valeur la plus elevée dans l'histo
threshold_normalized=(find(h.Values==max(h.Values(:))))/(size(h.Values,2));

%Dénormalisation de la valeur
threshold=denormaliseImage(threshold_normalized,0,255,min_Ifv,max_Ifv);

%Binaristion des spectres seuillées avec le seuil trouvé
Mb=image_binarization(Ifv,threshold);

%Visualisation des spectres seuillés
figure,imshow(Mb/255); 

%(b)
%Calcul de l’orientation dominante de Mb
[Ior,ordom] = orientationDominante(Mb);

%(c)
%Produire le texte en horizontal
Ir = rotationImage(I,360-ordom);
figure,imshow(Ir);
end
