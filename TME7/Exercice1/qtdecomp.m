function S = qtdecomp( I, thresh, mindim)
%Realise un decoupage en QuadTree de l'image I
%thresh est le seuil qui declenche le decouape
%mindim est la taimme minimale des blocs 
    [n,m] = size(I);
    if n ~= m
        error( 'L image doit etre carrée');
    end
    if n>mindim && std(I(:)) > thresh && mod(n,2) == 0
        n2 = n/2;        

        NO = qtdecomp( I(1:n2,1:n2), thresh, mindim);
        NE = qtdecomp( I(1:n2,n2+1:n), thresh, mindim);
        SO = qtdecomp( I(n2+1:n,1:n2), thresh, mindim);
        SE = qtdecomp( I(n2+1:n,n2+1:n), thresh, mindim);
        S = [NO,NE; SO, SE];
    else
        S = sparse(n,n);
        S(1,1) = n;
    end

end