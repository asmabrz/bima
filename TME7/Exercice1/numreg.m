function n=numreg(S)
  n=size(find(S~=0),1)
  %n=size(nonzeros(S),1) c'est une autre solution possible
end