function J=expand(I)



[n,m]=size(I);
maxValue=max([n,m]);
newDim = 2^(ceil(log2(maxValue)));
moyenne = floor(mean2(I));

J =ones(newDim,newDim).*moyenne;
J(1:n,1:m) = I

end