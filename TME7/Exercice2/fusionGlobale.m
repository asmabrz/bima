function F = fusionGlobale(S,I,thresh)
kmax = full(max(S));
sigma2 = 0; u = 0;n=0;
for k = 1 :kmax;
    [vals,i,j]=qtgetblk(I,S,k);
    if ~isempty(vals)
        for l = 1:length(i)
            n1 = n+i(l)*j(l);
            u1 = (u*n+mean2(vals(:,:,l)))/n1;
            sigma1 = (n*(sigma2+u*u)+i(l)*j(l)*( std2(vals(:,:,l)) + mean2(vals(:,:,l)^2) ))/n1;
            if sigma1 < thresh
                B(i(l):i(l)+k,j(l):j(l)+k) = 1;
            end
            n=n1;
            u=u1;
            sigma2=sigma1;
        end
    end
end
F = bwlabel(B);
end