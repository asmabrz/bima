clear all    
[I,n,m] = ouvrirImage('../Images/cygnus.tif');
J = expand(I);
S=qtdecomp(J,0.2,20);

F1= label2rgb(fusionLocale(S,J,11));
imageview(F1);


S2=qtdecomp(J,8,30);
q2 = quaddraw(J,S2);

F2= label2rgb(fusionGlobale(S2,J,142420));
imageview(F2);