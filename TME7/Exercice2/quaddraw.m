function [Q] = quaddraw(I,S)
Ir = I;
Ig = I;
Ib = I;
[r,c]=find(S);
kmax=size(r);
for k=1:kmax
    taille=S(r(k),c(k));
    for i = 1:taille
        for j = 1:taille
          if i==1 || j==1 || i==taille || j==taille
              Ir(i+r(k),j+c(k),1)=255;
              Ig(i+r(k),j+c(k),1)=0;
              Ib(i+r(k),j+c(k),1)=0;
          end
        end
    end
end
Q(:,:,1)=uint8(Ir);
Q(:,:,2)=uint8(Ig);
Q(:,:,3)=uint8(Ib);   
end