function F = fusionLocale(S,I,thresh)
kmax = full(max(S));
for k = 1 :kmax;
    [vals,r,c]=qtgetblk(I,S,k);
    if ~isempty(vals)
        for l = 1:length(r)
            if std2(vals(:,:,l)) < thresh
                B(r(l):r(l)+k,c(l):c(l)+k) = 1;
            end
        end
    end
end
F = bwlabel(B);
end